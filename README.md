# ![LabOS](https://gitlab.com/NickBusey/HomelabOS/raw/master/static/logo.png)

~~Your very own offline-first open-source data-center! Includes over 100 services!~~

## [Documentation](https://homelabos.com/docs/)

## [Installation](https://homelabos.com/docs/setup/installation/)

### [Installation Tutorial / Demo Video](https://youtu.be/p8cD349BGRI)

## Summary

This is a heavy modified version of homelabos made for a specific environment, if you need a working homelabos please download the original.
All credits go to [Nick Busey](https://nickbusey.com/).

## Goals

Deploy multiple containers to multiple hosts.

## Features

- ~~One command deployment `bash <(curl -s https://gitlab.com/NickBusey/HomelabOS/-/raw/master/install_homelabos.sh)`~~
- ~~Manual deployment - [Installation](https://homelabos.com/docs/setup/installation/#manual-set-up)~~
- Automated Backups
- Easy Restore
- Automated Tor Onion Service access
- Automated HTTPS via LetsEncrypt
- [Automated Settings Sync](https://homelabos.com/docs/setup/installation/#syncing-settings-via-git)

### [Planned Features](https://gitlab.com/NickBusey/HomelabOS/issues?label_name%5B%5D=enhancement)

## [Available Software](https://homelabos.com/docs/#available-software)

## Alternatives

- [HomeLabOS (original) ](https://homelabos.com/)
- [Ansible NAS](https://github.com/davestephens/ansible-nas)
- [DockSTARTer](https://dockstarter.com/)

## Get Support

- there is no support yet, maybe it wont be there at all, please download the original HomeLabOS

## Give Support

Suppport the original creator
[Become a Supporter on Patreon](https://www.patreon.com/nickbusey)

## [Contributing](https://homelabos.com/docs/development/contributing/)


