# HomelabOS

Welcome to LabOS! A Modified version of HomelabOS, all credits to Nick Busey (links below)

This stripped version; Labos aims to help deploy services on multiple hosts, with only changing the config files in the '/settings' directory.

HomelabOS is a collection of various separate services. You can find more information about each in the menu on the left.

## [Installation](setup/installation)

## [Getting Started](setup/gettingstarted)

## [Understanding Storage](setup/storage)

## Getting Help

If you are having problems you can:

- [File an issue on GitLab](https://gitlab.com/NickBusey/HomelabOS/issues).
- [Ask a question on HomelabOS Reddit](https://www.reddit.com/r/HomelabOS/)
- [Ask a question HomelabOS Zulip Chat](https://homelabos.zulipchat.com/)

## Available Software

### Categories

- [Analytics](#analytics)
- [Automation](#automation)
- [Blogging Platforms](#blogging-platforms)
- [Calendaring and Contacts Management](#calendaring-and-contacts-management)
- [Chat](#chat)
- [Docker & VM Management](#docker-vm-management)
- [Document Management](#document-management)
- [E-books](#e-books)
- [Email](#email)
- [Federated Identity/Authentication](#federated-identityauthentication)
- [Feed Readers](#feed-readers)
- [File Sharing and Synchronization](#file-sharing-and-synchronization)
- [Games](#games)
- [Gateways and terminal sharing](#gateways-and-terminal-sharing)
- [Media Streaming](#media-streaming)
- [Misc/Other](#miscother)
- [Money, Budgeting and Management](#money-budgeting-and-management)
- [Monitoring](#monitoring)
- [Note-taking and Editors](#note-taking-and-editors)
- [Password Managers](#password-managers)
- [Personal Dashboards](#personal-dashboards)
- [Photo and Video Galleries](#photo-and-video-galleries)
- [Read it Later Lists](#read-it-later-lists)
- [Social Networking](#social-networking)
- [Software Development](#software-development)
- [Task management/To-do lists](#task-managementto-do-lists)
- [VPN](#vpn)
- [Web servers](#web-servers)
- [Wikis](#wikis)

### Analytics

#### matomo
Open source analytics

### Automation

#### homeassistant
Home Assistant can automate just about any part of your home.

#### homebridge
HomeKit support for the impatient

#### kibitzr
Kibitzr acts as a replacement for IFTTT

### Blogging Platforms

#### ghost
Ghost is a fully open source, adaptable platform for building and running a modern online publication

### Calendaring and Contacts Management

### Chat

#### matterbridge
A program that allows users to link multiple chat platforms.

#### zulip
Threaded chat software

### Docker & VM Management

### Document Management

#### mayan
Mayan EDMS is a document management system.

#### paperless
Document management

### E-books

#### calibre
Ebook management system.

#### lazylibrarian
LazyLibrarian is a program to follow authors and grab metadata for all your digital reading needs.

### Email

#### mailu
Mailu is a simple yet full-featured mail server as a set of Docker images.

### Federated Identity/Authentication

#### keycloak
Open Source Identity and Access Management

#### openldap
LDAP management interface

### Feed Readers

#### freshrss
FreshRSS is a free, self-hostable aggregator.

#### miniflux
Miniflux is a minimalist and opinionated feed reader.

### File Sharing and Synchronization

#### duplicati
Free backup software to store encrypted backups online

#### jackett
Jackett provides API Support for your favorite torrent trackers.

#### lidarr
Sonarr but for Music.

#### minio
Minio is an S3 storage utility.

#### mylar
An automated Comic Book manager

#### nzbget
Efficient Usenet downloader.

#### ombi
Ombi is a self-hosted web application that automatically gives your shared Plex or Emby users the ability to request content by themselves!

#### overseerr
Overseerr is a request management and media discovery tool built to work with your existing Plex ecosystem.

#### sonarr
Smart PVR for newsgroup and bittorrent users.

### Games

#### factorio
Factorio headless server in a Docker container

#### minecraft
It's Minecraft.

#### minecraftbedrockserver
It's Minecraft.

#### quakejs
QuakeJS is a port of IOQuake3 to JavaScript with the help of Emscripten

### Gateways and terminal sharing

#### webvirtmgr
WebVirtMgr is a complete Kernel Virtual Machine (KVM) hypervisor manager.

### Media Streaming

#### airsonic
Airsonic is a free, web-based media streamer, providing ubiquitous access to your music.

#### beets
Beets is the media library management system for obsessive-compulsive music geeks.

#### emby
Emby is a media server

#### funkwhale
A social platform to enjoy and share music

#### jellyfin
Jellyfin is a media server. Just point it at your NAS collections of Movies and TV and you're off to the races.

#### massivedecks
Massive Decks is a comedy party game based on Cards against Humanity.

#### mstream
All your music, everywhere you go.

### Misc/Other

#### adguardhome
Network-wide software for blocking ads and tracking.

#### apache2


#### blank_on_purpose


#### chowdown
Simple recipes in Markdown format

#### cockpit


#### codimd
The best platform to write and share markdown

#### duckdns
DuckDNS is free dynamic DNS hosted on AWS.

#### erpnext
Open Source ERP for Everyone.

#### ethercalc
EtherCalc is a web spreadsheet

#### folding_at_home
Folding@home software allows you to share your unused computer power with scientists researching diseases.

#### gotify
A simple server for sending and receiving messages in real-time per WebSocket. (Includes a sleek web-ui)

#### grocy
ERP beyond your fridge - grocy is a web-based self-hosted groceries & household management solution for your home

#### grownetics
Growentics is an open source environmental mapping with plant management and tracking software suite.

#### guacamole


#### hubzilla
HubZilla is a powerful platform for creating interconnected websites featuring a decentralized/nomadic identity.

#### invidious
Invidious is an alternative front-end to YouTube

#### invoiceninja


#### mealie
Simple recipes in Markdown format

#### monicahq
Monica is an open source personal CRM.

#### n8n
n8n is a free and open node based Workflow Automation Tool.

#### nodered
Node-RED is a programming tool for wiring together hardware devices, APIs and online services in new and interesting ways.

#### octoprint
The snappy web interface for your 3D printer.

#### peertube


#### phpbb


#### pihole
Pi-hole provides network-wide ad blocking via your own Linux hardware.

#### piwigo


#### pixelfed


#### pleroma


#### plex


#### poli
An easy-to-use BI server built for SQL lovers. Power data analysis in SQL and gain faster business insights.

#### portainer


#### privatebin


#### qbittorrent


#### radarr


#### readarr


#### restic


#### rsshub


#### sabnzbd


#### samba


#### searx


#### seat


#### shinobi


#### sickchill


#### simplyshorten


#### snibox


#### speedtest


#### speedtest_tracker


#### statping


#### sui


#### syncthing


#### taisun


#### tautulli


#### teedy


#### thelounge


#### thespaghettidetective


#### tick


#### tiddlywiki


#### transmission


#### trilium


#### tubearchivist


#### turtl


#### ubooquity


#### unificontroller
The Unifi-controller Controller software is a powerful, enterprise wireless software engine ideal for high-density client deployments requiring low latency and high uptime performance.

#### watchtower
Watchtower is a process for automating Docker container base image updates

#### webdavserver
WebDAV Server A simple WebDAV service. If you enable WebDAV over SSL you will have a secure file transfer service setup, which is useful for e.g. syncing your notes from Joplin. If you don't need all the extra from NextCloud/OwnCloud, this service could be what you need.

#### webtrees
WebTrees is Online genealogy software

#### xfinityusageinfluxdb
xfinityusageinfluxdb is a service that runs on the xfinityusageinfluxdb server.

#### xteve
Xteve is an emulated TV Tuner for IPTV services. It offers guide management and smart filtering of channels from your IPTV provider.

#### zammad
Zammad is a web-based, open source user support/ticketing solution.

#### ztncui
ZeroTier network controller user interface

### Money, Budgeting and Management

#### firefly_iii
Firefly III is a money management app.

### Monitoring

#### elkstack
Elastic Search, Logstash and Kibana

#### grafana
Grafana is a Time Series Database graphing application.

#### healthchecks
A Cron Monitoring Tool written in Python & Django

#### huginn
Create agents that monitor and act on your behalf. Your agents are standing by!

#### netdata
Real-time performance monitoring, done right!

### Note-taking and Editors

#### bulletnotes
Open source note taking app

### Password Managers

#### bitwarden
Bitwarden is an open source password manager

### Personal Dashboards

#### heimdall
Heimdall Application Dashboard is a dashboard for all your web applications.

#### homedash
Homedash is a simple dashboard that allows to monitor and interact with many different services.

#### organizr
Access all your HomelabOS services in one easy place.

### Photo and Video Galleries

#### digikam
Professional Photo Management with the Power of Open Source

#### ownphotos
Self hosted Google Photos clone.

#### photoprism
PhotoPrism Personal Photo Management powered by Go and Google TensorFlow. Free and open-source.

### Read it Later Lists

#### wallabag
Wallabag Save and classify articles. Read them later. Freely.

### Social Networking

### Software Development

#### codeserver
Run VS Code on a remote server.

#### drone
Drone is a self-service continuous delivery platform

#### gitea
Git hosting platform

#### gitlab
Gitlab the single application for the entire DevOps lifecycle

#### jenkins
The leading open source automation server, Jenkins provides hundreds of plugins to support building, deploying and automating any project.

#### vikunja
Vikunja is a web-based project management tool

### Task management/To-do lists

#### wekan
Wekan Open is source Kanban board with MIT license

### VPN

#### openvpn
A Business VPN to Access Network Resources Securely

### Web servers

### Wikis

#### bookstack
Simple & Free Wiki Software


